<?php

namespace app\controllers;

use yii\web\Controller;

class IndexController extends Controller {

	public function beforeAction($action) {
		$this->enableCsrfValidation = false;
		return parent::beforeAction($action);
	}

	public function actionIndex() {
		
		if(!empty($_POST)){
			echo "<pre>";
			var_dump($_POST);
			exit();
		}
		
		return $this->render('index');
	}
	
}


