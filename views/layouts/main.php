<?php

use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
		<head>
				<meta charset="<?= Yii::$app->charset ?>">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<meta name="viewport" content="width=device-width, initial-scale=1">
				<?php $this->registerCsrfMetaTags() ?>
				<title><?= Html::encode($this->params['meta_title']) ?></title>
				<?php $this->head() ?>
		</head>
		<body>
				<?php $this->beginBody() ?>

				<div class="cbc">
						<div class="main">
								<header>
										<div class="center_box">
												<div class="wrapper">

														<div class="logo_box">
																<a href="<?= Yii::$app->params["rootUrl"] ?>"><img src="img/logo.png" alt="foodclub" /></a>
														</div>

														<div class="lng_box">
																<a href="<?= Yii::$app->params["rootUrl"] ?>" class="flag_ar"></a>
														</div>

														<div class="orders_counter">
																<span class="num">2<span class="line"></span></span>
																<span class="num">1<span class="line"></span></span>
																<span class="num">7<span class="line"></span></span>
																<span class="num">0<span class="line"></span></span>

																<span class="label">orders<br/>today</span>
														</div>

														<div class="r_box">
																<a href="tel:8800117117" class="h_phone"><span class="ico"></span><span>800 117-117</span></a>

																<div class="upan">
																		<div class="notice">
																				<a href="<?= Yii::$app->params["rootUrl"] ?>" class="ico_notice nftoggle"></a>

																				<nav class="utnav">
																						<ul>
																								<li class="item">
																										<span class="ico ico_1"></span>
																										<div class="text_box">
																												<h3>5 points</h3>
																												<p>Thank you for visiting FoodClub.<br/>You got 5 more points on your balance!</p>
																										</div>
																										<a href="<?= Yii::$app->params["rootUrl"] ?>" class="gcross"></a>
																								</li>
																								<li class="item">
																										<span class="ico ico_2"></span>
																										<div class="text_box">
																												<h3>5 points</h3>
																												<p>Thank you for visiting FoodClub.<br/>You got 5 more points on your balance!</p>
																										</div>
																										<a href="<?= Yii::$app->params["rootUrl"] ?>" class="gcross"></a>
																								</li>
																								<li class="item">
																										<span class="ico ico_3"></span>
																										<div class="text_box">
																												<h3>5 points</h3>
																												<p>Thank you for visiting FoodClub.<br/>You got 5 more points on your balance!</p>
																										</div>
																										<a href="<?= Yii::$app->params["rootUrl"] ?>" class="gcross"></a>
																								</li>
																								<li><a href="<?= Yii::$app->params["rootUrl"] ?>" class="more">Show more notifications</a></li>
																						</ul>
																				</nav>
																		</div>
																		<div class="navbox">
																				<a href="<?= Yii::$app->params["rootUrl"] ?>" class="midbox nftoggle">
																						<img src="imgc/user_ava_1_40.jpg" alt="" />
																						<span class="warrd"></span>
																				</a>
																				<nav class="utnav">
																						<ul>
																								<li class="points">600 points</li>
																								<li class="sepor"></li>
																								<li><a href="<?= Yii::$app->params["rootUrl"] ?>">Profile</a></li>
																								<li><a href="<?= Yii::$app->params["rootUrl"] ?>">Orders</a></li>
																								<li><a href="<?= Yii::$app->params["rootUrl"] ?>">Address</a></li>
																								<li><a href="<?= Yii::$app->params["rootUrl"] ?>">Settings</a></li>
																								<li class="sepor"></li>
																								<li><a href="<?= Yii::$app->params["rootUrl"] ?>" class="logout">Log Out</a></li>
																						</ul>
																				</nav>

																		</div>
																</div>
														</div>
														<div class="clear"></div>
												</div>
										</div>
								</header>

								<?=$content?>
								
						</div>

						<footer>
								<div class="center_box">
										<div class="wrapper">

												<nav class="f_nav">
														<ul>
																<li>
																		<a href="<?= Yii::$app->params["rootUrl"] ?>">
																				<span class="fadv_ico"><span class="ico_1"></span></span>
																				<span class="title">Rewards Program</span>
																				<span class="descr">We’re empowering businesses and teams to put Design first by helping them roll up their sleeves and apply customer-centric product.</span>
																		</a>
																</li>
																<li>
																		<a href="<?= Yii::$app->params["rootUrl"] ?>">
																				<span class="fadv_ico"><span class="ico_2"></span></span>
																				<span class="title">monthly lottery</span>
																				<span class="descr">We’re empowering businesses and teams to put Design first by helping them roll up their sleeves and apply customer-centric product.</span>
																		</a>
																</li>
																<li>
																		<a href="<?= Yii::$app->params["rootUrl"] ?>">
																				<span class="fadv_ico"><span class="ico_3"></span></span>
																				<span class="title">Restaurant Owners</span>
																				<span class="descr">We’re empowering businesses and teams to put Design first by helping them roll up their sleeves and apply customer-centric product.</span>
																		</a>
																</li>
																<li>
																		<a href="<?= Yii::$app->params["rootUrl"] ?>">
																				<span class="fadv_ico"><span class="ico_4"></span></span>
																				<span class="title">about foodclub</span>
																				<span class="descr">We’re empowering businesses and teams to put Design first by helping them roll up their sleeves and apply customer-centric product.</span>
																		</a>
																</li>
														</ul>
												</nav>
										</div>
								</div>
								<div class="bt_box">
										<div class="center_box">
												<div class="wrapper">
														<div class="soc_link">
																<a href="<?= Yii::$app->params["rootUrl"] ?>" class="apple"></a>
																<a href="<?= Yii::$app->params["rootUrl"] ?>" class="android"></a>
																<a href="<?= Yii::$app->params["rootUrl"] ?>" class="email"></a>
																<a href="<?= Yii::$app->params["rootUrl"] ?>" class="fb"></a>
														</div>
														<div class="copyright">
																<div>© 2014 Zomlex Inc. All rights reserved.</div>
																<nav>
																		<a href="<?= Yii::$app->params["rootUrl"] ?>">Partner Agreement</a>
																		<span>|</span>
																		<a href="<?= Yii::$app->params["rootUrl"] ?>">User Agreement</a>
																		<span>|</span>
																		<a href="<?= Yii::$app->params["rootUrl"] ?>">FAQ</a>
																		<span>|</span>
																		<a href="<?= Yii::$app->params["rootUrl"] ?>">Careers</a>
																</nav> 
														</div>
												</div>
										</div>
								</div>
						</footer>
				</div>

				<?php $this->endBody() ?>
		</body>
</html>
<?php $this->endPage() ?>
