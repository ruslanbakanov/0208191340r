<div class="center_box cb">
		<div class="uo_tabs cf">
				<a href="<?= Yii::$app->params["rootUrl"] ?>"><span>profile</a>
				<a href="<?= Yii::$app->params["rootUrl"] ?>"><span>Reviews</span></a>
				<a href="<?= Yii::$app->params["rootUrl"] ?>"><span>orders</span></a>
				<a href="<?= Yii::$app->params["rootUrl"] ?>" class="active"><span>My Address</span></a>
				<a href="<?= Yii::$app->params["rootUrl"] ?>"><span>Settings</span></a>
		</div>
		<div class="page_content bg_gray">
				<div class="uo_header">
						<div class="wrapper cf">
								<div class="wbox ava">
										<figure><img src="imgc/user_ava_1_140.jpg" alt="Helena Afrassiabi" /></figure>
								</div>
								<div class="main_info">
										<h1>Helena Afrassiabi</h1>
										<div class="midbox">
												<h4>560 points</h4>
												<div class="info_nav">
														<a href="<?= Yii::$app->params["rootUrl"] ?>">Get Free Points</a>
														<span class="sepor"></span>
														<a href="<?= Yii::$app->params["rootUrl"] ?>">Win iPad</a>
												</div>
										</div>
										<div class="stat">
												<div class="item">
														<div class="num">30</div>
														<div class="title">total orders</div>
												</div>
												<div class="item">
														<div class="num">14</div>
														<div class="title">total reviews</div>
												</div>
												<div class="item">
														<div class="num">0</div>
														<div class="title">total gifts</div>
												</div>
										</div>
								</div>
						</div>
				</div>

				<div class="uo_body">
						<div class="wrapper">
								<div class="uofb cf">
										<div class="l_col adrs">
												<h2>Add New Address</h2>

												<form method="POST">
														<div class="field">
																<label>Name *</label>
																<input name="name" required type="text" class="vl_empty" />
														</div>
														<div class="field">
																<label>Your city *</label>
																<select name="city" class="vl_empty" required>
																		<option class="plh"></option>
																		<option value="1">Babruysk</option>
																		<option value="2">San Francisco</option>
																</select>
														</div>
														<div class="field">
																<label>Your area *</label>
																<select name="area" required>
																		<option class="plh"></option>
																		<option>Good Area</option>
																		<option>Not So Good Area</option>
																</select>
														</div>

														<div class="field">
																<label>Street</label>
																<input name="street" type="text" value="" placeholder="" class="vl_empty" />
														</div>
														<div class="field">
																<label>House # </label>
																<input name="house" type="text" value="" placeholder="House Name / Number" />
														</div>

														<div class="field">
																<label class="pos_top">Additional information</label>
																<textarea name="additional"></textarea>
														</div>

														<div class="field">
																<input type="submit" value="add address" class="green_btn" />
														</div>
												</form>
										</div>

										<div class="r_col">
												<h2>My Addresses</h2>									

												<div class="uo_adr_list">
														<div class="item">
																<h3>HOME Address</h3>
																<p>Dubai, Business Bay Area, Sheikh Zayed Road, Single </p>
																<div class="actbox">
																		<a href="<?= Yii::$app->params["rootUrl"] ?>" class="bcross"></a>
																</div>
														</div>
														<div class="item">
																<h3>Work Address</h3>
																<p>Dubai, Business Bay Area, Sheikh Zayed Road, Single<br/>Business Tower, Suite 2204</p>
																<div class="actbox">
																		<a href="<?= Yii::$app->params["rootUrl"] ?>" class="bcross"></a>
																</div>
														</div>										
												</div>
										</div>
								</div>
						</div>
				</div>
		</div>
</div>